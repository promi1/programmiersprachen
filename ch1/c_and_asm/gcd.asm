global gcd
; int gcd(int u, int v);
gcd:
	; rdi = u
	; rsi = v
	cmp rsi, 0
	je gcd_v_0
    ; u % v
	mov rdx, 0
	mov rax, rdi
	idiv rsi
	; rdx = u % v

	; gcd(v, u % v)
	mov rdi, rsi ; v
	mov rsi, rdx ; u % v
	jmp gcd
gcd_v_0:
	mov rax, rdi
	ret


%if 0
  int gcd(int u, int v){
    if (v == 0){
        return u;
    }else{
        return gcd(v, u % v);
    }
}
%endif ; 0
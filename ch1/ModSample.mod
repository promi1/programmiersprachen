MODULE ModSample;

FROM InOut IMPORT WriteString, ReadInt, WriteInt, WriteLn;

PROCEDURE gcd(u, v: INTEGER): INTEGER;
BEGIN
  IF v = 0 THEN
    RETURN u;
  ELSE
    RETURN gcd(v, u MOD v);
  END;
END gcd;

VAR x, y: INTEGER;

BEGIN (* Hauptprogramm *)
  WriteString('Geben Sie zwei Integer ein:');
  WriteLn;
  ReadInt(x);
  WriteLn;
  ReadInt(y);
  WriteLn;
  WriteString('Der ggT von ');
  WriteInt(x, 1);
  WriteString(' und ');
  WriteInt(y, 1);
  WriteString(' ist ');
  WriteInt(gcd(x, y), 1);
  WriteLn;
END ModSample.

! A fortran95 program for G95
! By WQY
recursive function gcd(u, v) result (r)
    if(v == 0) then
        r = u
    else
        r = gcd(v, mod (u,  v))
    end if
end function gcd

program main
    integer :: x, y
    write(*,*)'Geben Sie zwei Integer ein:'
    read(*,*)x,y
    write(*,*)'der ggT von ', x, ' und ', y, ' ist ', gcd(x, y)
end
